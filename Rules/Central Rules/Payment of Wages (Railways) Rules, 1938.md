# The Payment of Wages (Railways) Rules, 1938

_Published vide Notification No.L. 3070(1), dated 5.5.1938, published in the Gazette of India, 1938, Part 1, page 943_

\[5th May, 1938\]

_In exercise of the powers conferred by sub-sections (2), (3) and (4) of section 26, read with section 24 of the Payment of Wages Act, 1936 (IV of 1936), and in supersession of the Payment of Wages (Railways) Rules, 1937, in so far as they relate to \[\* \* \*\] Railways within the meaning of the Government of India Act, 1935, the Central Government is pleased to make the following rules, the same having been previously published as required by sub-section (5) of section 26 of the first-named Act, namely:_

**1\. Title and application.** - (1) These rules may be called The Payment Of Wages (\[\* \* \*\] Railways) Rules, 1938.

\[(1-A) They extend to the whole of India \]\[\* \* \*\].

(2) These rules apply in respect of the payment of wages to persons employed upon any \[Railway (including factories) by or under a Railway Administration \]\[or by a contractor \]\[\* \* \*\].

**2\. Definitions.** - In these rules, unless there is anything repugnant in the subject or context,

(a) _"the Act"_ means the Payment of Wages Act, 1936 (IV of 1936);

(b) _"the authority"_ means an authority appointed under sub-section (1) of section 15 of the Act;

(c) _"the Court"_ means the Court mentioned in sub-section (1) of section 17 of the Act;

(d) _"deduction ;or breach of contract"_ means a deduction made in accordance with the provisions of the proviso to sub-section (2) of section 9;

(e) _"deduction for damage or loss"_ means a deduction made in accordance with the provisions of clause (c) of sub-section (2) of section 7;

\[\*\*\*\]

(f) _"Form"_ means a Form appended to these rules;

\[(ff)(i) "family" means-

(a) spouse, and

(b) children whether married or unmarried of railway servant in that order of precedence;

(ii) _"dependent relative"_ means-

(a) dependent parents;

(b) his/her deceased sons' widows and children;

(c) unmarried or widowed sister brother/ stepbrother provided residing with and wholly dependent on the railway servant;\]

(g) _"Inspector"_ means an Inspector authorised by or under section 14 of the Act;

(h) _"person employed"_ does not include any person to the payment of whose wages the Act does not apply;

(i) _"section"_ means a section of the Act;

(j) _"paymaster"_ means the Railway Administration or other person or persons who may be nominated as such by the Railway Administration under clause (c) of section 3 and in the case of a person employed by a contractor, the contractor;

(k) _"contractor"_ means a person fulfilling either directly or through a sub-contractor, a contract with a Railway Administration;

(l) _"employer"_ means the Railway Administration and in the case of persons employed by a contractor, the contractor;

\[(l) _"Chief Labour Commissioner (Central)"_ means officer appointed as such by the Central Government;

(ll) _"Regional Labour Commissioner (Central)"_ means an officer appointed as such by the Central Government;\]

(m) words and expressions defined in the Act shall be deemed to have the same meaning as in the Act.

**\[2A. Nominations.** - (1) The nomination from given by a railway servant in respect of payment of provident fund deposits shall be created as the nomination Form for the purpose of disbursement of unpaid wages.\]

**3\. Register of Fines.** - (1) On any railway where the employer has obtained approval under sub-section (1) of section 8 to a list of acts and omissions in respect of which fines may be imposed, the paymaster shall maintain a Register of Fines in Form I.

(2) At the beginning of a Register of Fines there shall be entered serially numbered the approved purpose or purposes on which the fines realised are to be expended.

(3) When any disbursements are made from the fines realised, a deduct entry of the amount so expended shall be made in the Register of Fines, and a voucher or receipt in respect of the amount shall be affixed to the register.

If more than one purpose has been approved, the entry of the disbursement shall also indicate the purpose for which it is made.

\[(4) Where no fine has been imposed on any employees in a wage period, a "nil" entry shall be made across the body of the register at the end of the wage period indicating also in precise terms the wage period to which the "nil" entry relates.\]

**4\. Register of deductions for damage or loss.** - (1) On every railway in which deductions for damage or loss are made, the paymaster shall maintain the Register required by sub-section (2) of section 10 in Form II.

\[(2) Where no deduction has been made from the wages of any employee in a wage period, a "nil" entry shall be made across the body of the register at the end of the wage period, indicating also in precise terms the wage period to which the "nil" entry relates.\]

**5\. Register of wages.** - A Register of wages shall be maintained by every employer \[\* \* \*\] in such form as the paymaster finds convenient. The said Register shall include the following particulars:

(a) the gross wages of each person employed for each wage period;

(b) all deductions made from those wages, with an indication, in each case, of the clause of sub-section (2) of section 7 under which the deduction is made;

(c) the wages actually paid to each person employed for each wage period and the date of payment.

**\[5A. Combined form of registers.** - Notwithstanding anything contained in these rules, where a combined (alternative) form is sought to be used by the employer to avoid duplication of work for compliance with the provisions of any other Act or the rules framed thereunder, an alternative suitable form in lieu of any of the forms prescribed under these rules, may be used with the previous approval of the Chief Labour Commissioner \]\[Central\].\]

**6\. Maintenance of Registers.** - The registers required by rules 3, 4, 4 \[, 5 and 18(3)\] , \[including registers maintained in lieu thereof in accordance with the provisions of rule 5-A\], shall be \[maintained upto date, kept and preserved for three years after the date of the last entry made in them, at the workspot and be produced before the Inspector on demand at the workspot\]:

\[Provided that the paymaster may with prior approval of the Regional Labour Commissioner (Central) preserve and keep the registers at a place other than the workspot:

Provided further that in the event of closure of any establishment of a Railway Administration or contractor, as the case may be, the pay master shall produce the registers before the Inspectors on demand in the office of the Inspector or any other place specified by him.\]

The registers shall normally be maintained in English, but where they are maintained in any other language than English, a true translation thereof in English shall be available.

**7\. Places for displaying notices.** - The \[Regional Labour Commissioner (Central)\] shall specify such place or places on the railway, other than factories, as he thinks fit (hereinafter referred to as "specified place" or "specified places") for the display of notices, lists and rules under rules 8,12 and 16.

**\[8.\]\[Notice of wage period, dates of payment and the names and addresses of Inspectors\].** - \[(1) The paymaster shall display in a conspicuous place at every station or establishment, within his jurisdiction, a notice in English and Hindi or in the language (if that be not Hindi) of the majority of the persons employed at such stations or establishments, as the case may be, showing,

(i) the wage period for which wages are payable;\]\[\* \* \*\]

\[(ii) the days or dates on which wages are to be paid;\]

\[(iii) the days or dates on which unpaid wages are to be paid; and

(iv) names and addresses of Inspectors having jurisdiction.\]

\[(2) Every such notice shall be maintained in a clean and legible condition.

(3) A copy each of every such notice and of any alteration therein shall be sent to the Inspector not less than two weeks in advance of the day or date on which wages are to be paid.\]

**\[8A. Supervision of payment.** - On direction of the Inspectors, the pay master or his representative at the workspot shall pay wages to the employed person under his supervision.\]

**9\. Prescribed authority.** - \[(1)\] The \[Regional Labour Commissioner (Central)\] shall be the authority competent to approve, under sub-section (1) of section 8, acts and omissions in respect of which fines may be imposed and, under sub-section (8) of section 8, the purposes to which the proceeds of fines shall be applied.

\[(2) The Chairman of the Staff Benefit Fund Committee functioning in each zonal railway, production units of the railways and other railway organisations, independent of a zonal railway, shall be the prescribed authority with whom the amount required to be deposited tinder clause (b) of sub-section (1) of section 25-A of the Act shall be deposited and who shall deal with the amount so deposited in the manner prescribed in rule 18-B.\]

**10\. Application in respect of fines.** - Every employer requiring the power to impose fines in respect of any acts and omission on the part of employed persons shall send to the \[Regional Labour Commissioner (Central)\],

(a) a list, in English, in duplicate, clearly defining such acts and omissions;

(b) in cases where the Railway Administration itself does not intend to be the sole authority empowered to impose fines, a list, in duplicate, showing by virtue of office such of his officers as may pass orders imposing fines and the class of establishment on which any such officer may impose fine.

**11\. Approval of list of acts and omission.** - The \[Regional Labour Commissioner (Central)\] may, on receipt of the list prescribed in sub-rule (a) of rule 10, and after such enquiry as he considers necessary, pass orders in respect of the list referred to in clause (a) of rule 10 either,

(a) disapproving the list,

(b) approving the list either in its original form or as amended by him, in which case such list shall be deemed to have been approved under sub-section (1) of section 8:

Provided that no order disapproving or amending the list shall be passed unless the employer shall have been given an opportunity of showing cause orally or in writing against such order.

**12\. Posting of list.** - The employer shall display at or near the main entrance of every factory, and at the specified place or specified places, a copy in English, together with a literal translation thereof, in the language of the majority of the persons employed at such factory or place, of the list of acts and omissions approved by the Authority prescribed under rule 9.

**13\. Persons authorised to impose fines.** - (1) No fine may be imposed upon a person employed by a Railway Administration by any person other than the Railway Administration, or by a person holding an appointment named in the list referred to in clause (b) of rule 10.

(2) In the case of persons employed by a contractor, no fine may be imposed by any person other than the contractor:

Provided that a contractor who runs more than one establishment in two or more localities, and who employs not less than fifty persons in one locality, may with the approval of the Supervisor, delegate his power to fine to his representative in that locality.

**\[14. Procedure for imposing fines and deductions for loss or damages.** - (1) No fine shall be imposed and no deductions for damage or loss under sub-section (1) of section 10 of the Act shall be made from a person employed by a Railway Administration except in accordance with the procedure laid down in the rules in force on the Railway and until the employed person has been given an opportunity of showing cause against such imposition or deduction.\]

(2) No fine shall be imposed on and no deduction for damage or loss shall be made from the wages of a person employed by a contractor until the person authorised to impose the fine or make the deduction has explained personally to the said person the act or omission, or damage or loss in respect of which the fine or deduction is proposed to be imposed, and the amount of the fine or deduction, which it is proposed to impose, and has heard his explanation in the presence of at least one other person.

**15\. Information to paymaster.** - The person imposing a fine or directing the making of a deduction for damage or loss shall (unless such person is the paymaster) at once inform the paymaster of all particulars necessary for the completion of the register prescribed in rule 3 or rule 4, as the case may be.

**16\. Deductions under the proviso to sub-section (2) of section 9.** - (1) No deduction under the proviso to sub-section (2) of section 9 shall be made from the wages of an employed person who is under the age of fifteen years or is a woman.

(2) No such deduction shall be made from the wages of any employed person unless

(a) there is provision in writing in the terms of the contract of employment requiring him to give notice of the termination of his employment; and

(i) the period of this notice does not exceed fifteen days or the wage period, whichever is less; and

(ii) the period of this notice does not exceed the period of notice which the employer is required to give of the termination of that employment;

(b) this rule has been displayed in English and in the language of the majority of the employed persons at or near the main entrance of the factory, and at the specified place or specified places concerned, and has been so displayed for not less than one month before the commencement of the absence in respect of which the deduction is made;

(c) notice has been displayed at or near the main entrance of the factory and at the specified place or specified places, concerned, giving the names of the persons from whom the deduction is proposed to be made, the number of days' wages to be deducted and the conditions (if any), on which the deduction will be remitted:

Provided that where the deduction is proposed to be made from all the persons employed in any departments or sections or factories of the Railway, it shall be sufficient, in lieu of giving the names of the persons in such departments, sections or factories, to specify the departments, sections or factories affected.

(3) No such deduction shall exceed the wages of the person employed for the period by which the notice of termination of service given falls short of the period of such notice required by the contract of employment.

(4) If any conditions have been specified in the notice displayed under clause (c) of sub-rule (2), no such deduction shall be made from any person who has complied with such conditions.

**\[17. Annual return.** - Every employer shall, on or before the 1st day of February in each year, upload unified annual return in Form III on the web portal of the Central Government in the Ministry of Labour and Employment giving information as to the particulars specified in respect of the preceding year:

Provided that during inspection, the inspector may require the production of accounts, books, registers and other documents maintained in electronic form or otherwise.

_Explanation._ - For the purposes of this rule, the expression _"electronic form"_ shall have the same meaning as assigned to it in clause (r) of section 2 of the Information Technology Act, 2000 (21 of 2000).\]

**18\. Advances to persons employed by a contractor.** - (l) An advance of wages not already earned shall not ordinarily exceed the amount equal to two calendar months' wages of the employed person. In exceptional cases the amount of such advance may, with the previous sanction of the \[Regional Labour Commissioner (Central)\], be made to the extent of four calendar months' wages.

(2) The advance may be recovered in instalments by deductions from wages spread over not more than twelve months in the case of ordinary advance and twenty months in the case of special advance. In no case shall the amount of instalment exceed one fourth of the wages earned in one month.

(3) The amounts of all advances sanctioned and the repayments thereof shall be entered in a register in Form V.

**\[18A. Deposit of amount of undisbursed wages.** - In respect or the staff on Indian Railways including Production Units and their subordinate establishments, all amount of undisbursed wages remaining of his whereabouts not being known, shall be transferred to the Staff Benefit Fund by the Accounts Officer after the expiry of the three years.

(2) The amount referred to in sub-rule (1) shall be deposited by the paymaster together with relevant details in Form VI to the Chairman, Staff Betlefit Fund.

**18B. Manner of dealing with amounts.** - (1) The amounts deposited without Chairman, Staff Benefit Fund under rule 18-A shall remain with the Chairman of the Fund for four years.

(2) As soon as possible the Railway Administration will publish in any two Newspapers circulated in the language commonly understood in the area in which the factory or the establishment in which the undisbursed wages were earned, is situated and will exhibit them on notice-board of the factory or establishment.

(3) The prescribed authority shall release the money to the nominee or to that person who has claim to this money and which has been decided by the competent authority Court.

**18C.** The amount deposited under rule 18-A shall be applied by the Chairman of the Staff Benefit Fund to met the expenditure for the purpose prescribed for Staff Benefit Fund.

**18D. The family member/dependent relative of employed person to make claims.** - The spouse or children and where the employed person does not have spouse or children, the dependent relative in the order of precedence given in clause (ff)(it) of rule 2 can make claims of the amount at any time within seven years during which time the wages remain with the paymaster for a period of first three years, as specified in rule 18-A(1) and then credited to the Staff Benefit Fund. In such cases, only the principal amount as remaining with the paymaster deposited by the paymaster with the Chairman, Staff Benefit Fund Committee shall be payable to the claimant.\]

**19\. Procedure, costs and court-fees.** - The procedure to be followed by the authorities appointed under sub-section (1) of section 15 and the Courts mentioned in sub-section (1) of section 17 of the Act, the scales of costs which may be allowed in, and the amount of court-fees payable in respect of proceeding under the Act to which these rules apply shall be such procedure, scales an d amounts as are from time to time, prescribed by the State Government in the exercise of its powers under the Act in that behalf for the authority or Court concerned.

**\[19A. Payment of undisbursed wages.** - (1) If all amounts payable to an eft loyed person as wages could not or cannot be paid on account of his death woore payment or on account of his whereabouts not being known, shall be ,paid to the person nominated by him in this behalf.

(2) Where wages remain undisbursed because no nomination had been made by the employed person or for any reason such amounts could not be paid to the nominee of the employed person until the expiry of three years from: the date the same had become payable all amounts of such undisbursed wages shall be transferred by the Accounts Officer to the Staff Benefit Fund before the expiry of the fifteenth day after the last day of the said period of three years.**19B. Manner of dealing with deposited amounts.** - The amounts or undisbursed wages deposited with the Chairman Staff Benefit Fund shall be applied by him to meet the expenditure for the purpose prescribed for the Staff Benefit Fund.\]

**20\. Abstracts.** - The abstracts of the Act and of the rules made thereunder to be displayed under section 25 shall be in Form IV.

**21\. Penalties.** - Any breach of rules 3, 4, 5, \[5-A\], 6, 8,12,15, \[17 and 18(3)\]of these rules shall be punishable with fine which may extend to two hundred rupees.

Form I

(See rule 3)

Register of Fines

_Railway-Department/Division/District/Factory_

Sl.No

Name

Father's name

Department

 Act or omission for which fine imposed

Whether workman showed cause against fine or not, if so, enter date

 Rate of wages

 Date and amount of fine imposed

 Date on which fine realized

 Remarks

1

2

3

4

5

6

7

8

9

10

Form II

(See rule 4)

_Register of Deductions for Damage or Loss Caused to the Employer by the Neglect or Default of the Employed Persons_

_Railway-Department/Division/District/Factory_

Sl.No

 Name

Father's name

Department

Damage or loss caused

 Whether worker showed cause against deduction or not, if so, enter date

Date and amount of deduction imposed

No. of instalments, if any

 Date on which total amount realized

Remarks

1

2

3

4

5

6

7

8

9

10

\[Form III\]

**\[See rule 17\]**

**Unified Annual Return**

**A. General Part:**

**Particulars:**

**(a) Name of the establishment**       

**Address of the establishment.** House no./Flat No.      Street/Plot No.   

Town      District      State      Pin Code   

**(b) Name of the employer**       

**Address of the employer.** House no./Flat No.      Street/Plot No.   

Town      District      State      Pin Code   

E-mail ID      Telephone Number      Mobile Number   

**(c) Name of the manager or person responsible for supervision and control of establishment**       

**Address.** House no./Flat No.      Street/Plot No.   

Town      District      State      Pin Code   

E-mail ID      Telephone Number      Mobile Number   

**B. Employer's Registration/ License number under the Acts mentioned in column (2) of the table below:**

  

S. No.

Name

Registration

If yes (Registration No.)

(1)

(2)

(3)

(4)

01.

The Building and other Construction Workers (Regulation of Employment & Conditions of Service) Act, 1996.

  

  

  

02.

The Contract Labour (Regulation & Abolition) Act, 1970.

  

  

  

03.

The Inter-State Migrant Workmen (Regulation of Employment and Condition of Service) Act, 1979.

  

  

  

04.

The Employees Provident Funds and Miscellaneous Provisions Act, 1952.

  

  

  

05.

The Employees’ State Insurance Act, 1948.

  

  

  

06.

The Mines Act, 1952.  
Notice of opening under Regulation 3 of Coal Mines Regulation, 1957 or Regulation 3 of Metalliferous Mines Regulation, 1961.

  

  

  

07.

The Factories Act, 1948.

  

  

  

08.

The Motor Transport Workers Act, 1961.

  

  

  

09.

The Shops and Establishments Act (State Act).

  

  

  

10.

Any other Law for the time being in force.

  

  

  

**C. Details of Principal Employer, Contractor and Contract Labour:**

  

01.

Name of the principal employer in the case of a contractor’s establishment.

  

02.

Date of commencement of the establishment.

  

03.

Number of Contractors engaged in the establishment during the year.

  

04.

Total Number of days during the year on which Contract Labour was employed.

  

05.

Total number of man-days worked by Contract Labour during the year.

  

06.

Name of the Manager or Agent (in case of mines).

  

07.

**Address** House No./Flat No.      Street/Plot No.      Town   

District      State      Pin Code   

E-mail ID      Telephone Number      Mobile Number   

**D. Working hours and weekly rest day:**

  

01.

Number of days worked during the year.

  

02.

Number of mandays worked during the year.

  

03.

Daily hours of work.

  

04.

Day of weekly holiday.

  

**E. Maximum number of persons employed in any day during the year:**

  

Sl. No.

Males

Females

Adolescents (between the age of 14 to 18 years.)

Children (below 14 years of age.)

Total

  

  

  

  

  

  

**F. Wage rates (Category Wise):**

  

Category

Rates of Wages

No. of workers

Regular

Contract

Male

Female

Children

Adolescent

Male

Female

Children

Adolescent

Highly

  

  

  

  

  

  

  

  

  

Skilled

  

  

  

  

  

  

  

  

  

Skilled

  

  

  

  

  

  

  

  

  

Semiskilled

  

  

  

  

  

  

  

  

  

Unskilled

  

  

  

  

  

  

  

  

  

**G. (a) Details of Payments:**

  

Gross wages paid

Deductions

Net wages paid

In cash

In kind

Fines

Deductions for damage or loss

Others

In cash

In kind

  

  

  

  

  

  

  

**(b) Number of workers who were granted leave with wages during the year:**

  

Sl. No.

During the year

Number of workers

Granted leave with wages

  

  

  

  

**H. Details of various welfare amenities provided under the statutory schemes:**

  

Sl. No.

Nature of various welfare amenities provided

Statutory (specify the statute)

  

  

  

**Declaration**

It is to certify that the above information is true and correct and also I certify that I have complied with the all provisions of Labour Laws applicable to my establishment.

Place          Date          Sign. Here    

Form IV

Abstract of the Payment of Wages Act, 1936, and the Rules Made Thereunder

_Whom the Act affects_

**1.** The Act applies to the payment of wages to persons in this factory receiving less than \[Rs. 1,6001\] a month.

**2.** No employed person can give up by contractor agreement his rights under the Act.

_Definition of wages_

**3.** "Wages" means all remuneration (whether by way of salary, allowances or otherwise) expressed in terms of money or capable of being so expressed which would, of the terms of employment, express or implied, were fulfilled be payable to a person employed in respect of his employment or of work done in such employment, and includes

(a) any remuneration payable under any award or settlement between the parties or order of a Court;

(b) any remuneration to which the person employed is entitled in respect of overtime work or holidays or any leave period;

(c) any additional remuneration payable under the terms of employment (whether called a bonus or by any other name);

(d) any sum which by reason of the termination of employment of the person employed is payable under any law, contract or instrument which provides for the payment of such sum, whether with or without deductions but does not provide for the time within which the payment is to be made;

(e) any sum to which the person employed is entitled under any scheme framed under any law for the time being in force,

but does not include,

(1) any bonus (whether under a scheme of profit sharing or otherwise) which does not form part of the remuneration payable under the terms of employment or which is not payable under any award or settlement between the parties or order of a Court;

(2) the value of any house-accommodation, or the supply of light, water, medical attendance or other amenity or of any service excluded from the computation of wages by a general or special order of the State Government;

(3) any contribution paid by the employer to any pension or provident fund, and the interest which may have accrued thereon;

(4) any travelling allowance or the value of any travelling concessions;

(5) any sum paid to the employed person to defray special expenses entailed on him by the nature of his employment; or

(6) any gratuity payable on the termination of employment in cases other than those specified in sub-clause (d).

_Responsibility for and method of payment_

4\. The railway administration or a person nominated by the railway administration for the concerned local area is responsible for the payment under the Act of wages to persons employed under him, and any contractor employing persons is responsible for payment to the persons he employs.

5\. Wage periods shall be fixed for the payment of wages at intervals not exceeding one month.

6\. Wages shall be paid on a working day within 7 days of the end of the wage period (or within ten days if 1,000 or more persons are employed).The wages of a person discharged shall be paid not later than the second working day after his discharge.

7\. Payment in kind is prohibited.

_Fines and Deductions_

8\. No deductions shall be made from wages except those authorised under the Act (See paragraphs 9-15 below).

9\. (1) Fines can be imposed only for such acts and omissions as the employer may, with the previous approval of the Supervisor, specify by a notice displayed at or near the main entrance of the factory and after giving the employed person an opportunity for explanation.

(2) Fines

(a) shall not exceed \[three per cent. of the wages payable to him in respect of that wage period in which fine is imposed\];

(b) shall not be recovered by instalments, or later than sixty days of the date of imposition;

(c) shall be recorded in a register and applied to such purposes beneficial to the employed persons as are approved by the Supervisor;

(d) shall not be imposed on a child.

10\. (a) Deductions for absence from duty can be made only of account of the absence of the employed person at times when he should be working and such deductions must not exceed an amount which is in the same proportion to his wages for the wage period, as the time he was absent in that period is to the total time he should have been at work.

(b) If ten or more employed persons, acting in concert, absent themselves without reasonable cause and without due notice, the deduction for absence can include wages for eight days in lieu of notice, but

(1) No deduction for breaking a contract can be made from a person under 15 or a woman;

(2) There must be a provision in writing which forms part of the contract of employment requiring that a specific period of notice of intention to cease work not exceeding 15 days or the period of notice which the employer has to give to discharge a worker, must be given to the employer and that wages may be deducted in lieu of such notice;

(3) The above provision must be displayed at or near the main entrance of the factory;

(4) No deduction of this nature can be made until a notice that this deduction is to be made has been posted at or near the main entrance of the factory;

(5) No deduction must exceed the wages of the employed person for the period by which the notice he gives of leading employment is less than the notice he should have give under his contract.

11\. Deductions can be made for damage to or loss of goods expressly entrusted to an employed person or for loss of money for which he is required to account, where such damage or loss is due to his neglect or default.

Such deductions cannot exceed the amount of the damage or loss caused and can be made only after giving the employed persons an opportunity for explanation.

12\. Deductions can be made, equivalent to the value thereof, for house accommodation supplied by the employer or by Government or any housing board set up under any law for the time being in force (whether the Government or the board is the employer or not) or any other authority engaged in the business of subsidising house accommodation which may be specified in this behalf by the Central Government, amenities and services (other than tools and raw materials) supplied by the employer: provided these are accepted by the employed person as a part of the terms of his employment and have in the case of amenities and services been authorised by order of the Central Government.

13\. (a) Deductions can be made for the recovery of advance or for adjustment of over-payment of wages.

(b) Advances made before the employment began can only be recovered from the first payment of wages for a complete wage period but no recovery can be made of advances given for travelling expenses before payment began.

(c) Advances of unearned wages can be made at the paymaster's discretion during employment.

14\. Deductions can be made for subscription to and for repayment of advances from any recognised provident fund.

15\. Deductions can be made for payments to co-operative societies approved by the Central Government or to the postal insurance, subject to any conditions imposed by the Central Government.

Deductions can also be made with the written authorisation of the person employed for payment of any premium on his life insurance policy to the Life Insurance Corporation of India or for the purchase of securities of the Government of India or of any State Government or for being deposited in any Post Office Savings Bank in furtherance of any savings scheme of any such Government.

15-A. Any loss of wages resulting from withholding of increment or promotion, reduction to a lower post or time scale or to a lower stage in a time scale or suspension does not constitute deduction from wages within the meaning of the Act. For this purpose the rules framed by an employer in relation to his employees in Railways for the purpose of any of the aforesaid penalties shall provide that:

(i) any such penalty as aforesaid, except the penalty of suspension, shall not be imposed unless the person concerned

(a) has been informed of the charges in respect of which it is proposed to impose the penalty;

(b) has been given a reasonable opportunity of showing cause why the proposed penalty should not be imposed.

(ii) the person concerned is given a right of appeal against any order imposing the penalty.

_Inspections_

16\. An Inspector can enter on any premises and can exercise the powers of inspection(including examination of documents and taking of evidence) as he may deem necessary for carrying out the purposes of the Act.

_Complaints of Deductions or Delays_

17\. (1) Where irregular deductions are made from wages, or delays in payment take place, an employed person can make an application in the prescribed form within 6 months to the Authority appointed by the State Government for the purpose. An application delayed beyond this period may be rejected unless sufficient cause for delay is shown.

(2) Any legal practitioner, official of a registered trade union, Inspector under the Act or other person acting with the permission of the authority can make the complaint on behalf of an employed person.

(3) A single application may be presented by, or on behalf of any number of persons belonging to the same factory, the payment of whose wages has been delayed.

_Family member or dependant relations of employed person can make claims_

17-A. The spouse or children and where the employed person does not have spouse or children, the dependent relation can make claims at any time within seven years during which time the wages remain with the paymaster for a period of 1st three years.

_Action by the Authority_

18\. The Authority may award compensation to the employed person in addition to ordering the payment of delayed wages or the refund of illegal deductions.If a malicious or vexatious complaint is made, the Authority may impose a penalty not exceeding Rs. 50 on the applicant and order that it be paid to the employer.Appeal against the Authority

19\. An appeal against an order dismissing either wholly or in part an application or against a direction may be preferred within thirty days in Madras, Bombay, Calcutta to the Court of Small Causes and elsewhere to the District Court

(a) if the total amount directed to be paid exceeds Rs. 300;

(b) by an employed person or any official of a registered trade union authorised in writing to act on his behalf, if the total amount of wages withheld from him or his co-workers, exceeds rupees fifty;

(c) by a person directed to pay a penalty for a malicious or vexatious application.

_Payment of undisbursed wages_

19-A. If all amounts payable to an employed person as wages could not be or cannot be paid on account of his death before payment or an account of his whereabouts not being known, shall be paid to the person nominated by him in this behalf.

_Punishments for breaches of the Act_

20\. Anyone delay in the payment of wages beyond the due date, or making any unauthorised deductions from wages is liable to a fine up to Rs. 500, only if prosecuted with the sanction of the Authority or the Appellate Court.

21\. The paymaster who

(1) does not fix a wage period, or

(2) makes payment in kind, or

(3) fails to display at or near the main entrance of the factory this abstract in English and in the language of the majority of the employed persons, or

(4) breaks certain rules made under the Act, is liable to find not exceeding Rs. 200. A complaint to this effect can be made only by the Inspector, or with his sanction.

Form V

\[See rule 18(3)\]

Register of Advances Made to Employed Persons

Name of Contractor ..............................

Sl No

Name

Father's name

Department

Date and amount of advance made

Purpose(s) for which advance made

No.of instalments by which advance to be repaid

Postponements granted

Date on which total amount repaid

Remarks

1

2

3

4

5

6

7

8

9

10

Form VI

\[See sub rule (2) of rule 18-A\]

From

(Give here name and complete address of the paymaster)

To

The Chairman, Staff Benefit Fund

_Subject: Transfer of amount of undisbursed wages_

Sir,

As required under sub-rule (1) read with sub-rule (2) of rule 18-A of the Payment of Wages (Railways) Rules, 1938, I transfer the amount of Rs ............................................................

(Amount in figures)

(Rupees ...........................................................) from Suspense Head "Deposit unpaid wages" to

(Amount in words)

Staff Benefit Fund.

The above mentioned amount represent all amounts payable as wages to person(s) enlisted in Annexures I & II employed in ..............................(mention the name and address of the establishment) which remained undisbursed because either no nomination had been made by the employed person(s) or for any reasons such amounts could not be paid to the respective nominee(s) of the employed person(s). The relevant details are furnished hereunder

1. Particulars of the relevant wage period

..........................................................

(Mention the details of the wage period).

2. Number of cases in which all amounts payable to an employed person as wages remained undisbursed for want of nomination (details as per Annexure I);

...........................................................

(Mention the number of such cases)

3. Number of cases in which all amounts payable to an employed person as wages could not be paid to person (s) nominated by employed person (s)

..........................................................

(Mention in the number)

.............................................

Signature of the paymaster/Officer

authorised

Designation

Name and address of the

Establishment or rubber stamp thereof.

Place..................

Date...................

Annexure I

Sl.No.

Name and address of the employee

Wage period

Amount payable

(1)

(2)

(3)

(4)

Annexure II

Sl. No

Name and address of the employee

Name and address of the nominee(s)

Wage period

Amount Payable

(1)

(2)

(3)

(4)

(5)

