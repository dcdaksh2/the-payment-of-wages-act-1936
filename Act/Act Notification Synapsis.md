<table>
<tr>
    <th>Notification Date</th>
        <td>26-06-1996</td>
</tr>
<tr>
    <th>Notification Title</th>
        <td>Tamilnadu-Extension of provisions of Payment of Wages Act to shops and commercial establishments employing 20 or    more persons. 26th June, 1996</td>
</tr>
<tr>
    <th>Notification Description</th>
        <td>**Government of  Tamil Nadu No. II(2)JLE/1 624/96**   In exercise of the powers conferred by sub- clause (h) of clause (ii) of section 2 of the Payment of Wages Act, 1936 (Central Act IV of 1936), the Governor of Tamil Nadu having regard to the nature of the Shops and Establishments defined in clauses (3), (6) and (16) of section 2 of the Tamil Nadu Shops and Establishments Act, 1947 (Tamil Nadu Act XXXVI of 1947), employing 20 or more persons and the need for protection of persons employed therein, hereby specifies the said Shops and Establishments as Industrial Establishments for the purpose of the Payment of Wages Act, 1936 (Central Act IV of 1936);

And, in exercise of the powers conferred by sub-section (5) of section 1 of the Payment of Wages Act, 1936 (Central Act IV of 1936), and in supersession of the Labour and Employment Department Notification No. II (2)JLE/5770/90, dated the 30th October, 1990, published at page 647 of Part II—Section of the _Tamil Nadu Government Gazette_, dated the 21st November, 1990. the Governor of Tamil Nadu hereby extends the provisions of the said Act to the aforesaid industrial establishments, the preliminary notification of which has already been published as required by subsection (5) of section 11 of the said Act. </td>
</tr>
<tr>
<th>Notification Issues By</th>
        <td>State Government</td>
</tr>
</table>

